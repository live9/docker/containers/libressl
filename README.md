# LibreSSL

Libre SSL with options as environment variables. By default will create a self-signed certificate.

## Certificate creation params as env vars
* SSL_VALID_DAYS
* SSL_SUBJ_COUNTRY
* SSL_SUBJ_STATE
* SSL_SUBJ_LOCALITY
* SSL_SUBJ_ORG
* SSL_SUBJ_ORG_UNIT
* SSL_SUBJ_CN
* SSL_SUBJECT
* SSL_KEY_LENGTH

## Docker compose example with Nginx
* libressl container generate certs in volume named certs mounted at `/etc/ssl/`
* nginx container also mount certs volume in `/etc/ssl/`
  * entrypoint wait until  /etc/ssl/dhparam.pem is present
  * entrypoint generates ssl conf from template and start nginx

docker-compose.yml
```
version: '3'

volumes:
  certs:

services:
  libressl:
    image: registry.gitlab.com/live9/docker/images/libressl
    volumes:
    - 'certs:/etc/ssl/'
  nginx:
    image: nginx:1.13.0-alpine
    command: ["nginx", "-g", "daemon off;"]
    entrypoint: ["/usr/local/bin/entrypoint"]
    working_dir: /etc/nginx
    volumes:
    - './nginx/entrypoint:/usr/local/bin/entrypoint:ro'
    - './nginx/default.conf.template:/etc/nginx/default.conf.template:ro'
    - 'certs:/etc/ssl/:ro'
```

entrypoint
```
#!/bin/sh
set -e

function _render_template() {
        local template=$1
        local out_file=$2
        echo "Running template file ${template}"
        eval "echo \"$(cat ${template})\"" > ${out_file}
        echo "Done."
}


echo "ENTRYPOINT start"
until [ "`wc -c /etc/ssl/dhparam.pem |cut -d ' ' -f1`" -gt 0 ]; do
	echo "Waiting for selfsigned certificate to be ready..."
	sleep 5
done
if [[ -z $SERVER_NAME ]]; then SERVER_NAME=localhost; fi
echo SERVER_NAME is $SERVER_NAME
_render_template  /etc/nginx/default.conf.template /etc/nginx/conf.d/default.conf
cat /etc/nginx/conf.d/default.conf
echo "ENTRYPOINT end"

exec "$@"
```

default.conf.template
```
# Shared SSL/TLS conf for all server sections (vhosts)
ssl on;
ssl_prefer_server_ciphers on;
ssl_session_cache  builtin:1000  shared:SSL:10m;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_ciphers	'EDH+CAMELLIA:EDH+aRSA:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA256:EECDH:+CAMELLIA128:+AES128:+SSLv3:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!DSS:!RC4:!SEED:!IDEA:!ECDSA:kEDH:CAMELLIA128-SHA:AES128-SHA';
ssl_dhparam /etc/ssl/dhparam.pem;
add_header Strict-Transport-Security max-age=15768000; # six months
ssl_certificate /etc/ssl/selfsigned.crt;
ssl_certificate_key /etc/ssl/selfsigned.key;

server {
        server_name ${SERVER_NAME};
        listen *:443;
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;
	charset utf-8;
        root /app;

        location = /favicon.ico {
                log_not_found off;
                access_log off;
        }

        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }
}
```
