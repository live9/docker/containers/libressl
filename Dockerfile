FROM alpine:3.12.0
LABEL maintainer="Juan Luis Baptiste <juan.baptiste@karisma.org.co>"
LABEL maintainer="Fredy P. <digitalfredy@live9.org>"

COPY entrypoint /usr/bin
RUN apk add --update libressl && \
    rm -rf /var/cache/apk/* && \
    chmod 755 /usr/bin/entrypoint && \
    cp -a /etc/ssl/openssl.cnf /

CMD ["entrypoint"]
